const debounce = (func, delay) => {
    let timeoutId;
    // console.log(prem);
    return (...args) => {
        if(timeoutId){
            clearTimeout(timeoutId);
        }
        timeoutId = setTimeout(() => {
            func.apply(null, args);
        },delay);
    };
};